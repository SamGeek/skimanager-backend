/**
 * Stay.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    // String name;
    // String code;
    // String image;
    // String startDate;
    // String endDate;
    // String stayType;
    // int reservedPlaces;
    // int totalPlaces;

    name: { type: 'string', required: true, },
    code: { type: 'string', required: true, },
//    image: { type: 'string', required: true, },
    startDate: { type: 'string', required: true, },
    endDate: { type: 'string', required: true, },
    stayType: { type: 'string', required: true, isIn: [ 'pension_complete', 'gestion_libre'],},
    reservedPlaces: { type: 'number', defaultsTo : 0 },
    totalPlaces: { type: 'number', required: true, },

    /** etat du séjour
     * permet de le cloturer ou de le laisser ouvert
    */
    state: { type: 'String', },


    //photo
    avatarUrl: { type: 'string',  },
    avatarName: { type: 'string',  },
    avatarFd: { type: 'string',  },


    //le booker cest celui qui crée le séjour
    //ce champ doit etre requis
    booker: {
      model: 'user',
      required: true
    },

    users: {
      collection: 'userstay',
      via: 'stay'
    },

    usersys: {
      collection: 'user',
      via: 'stays'
    },

    // Add a reference to Stay //peut etre a suppri
    user: {
      model: 'user',
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

