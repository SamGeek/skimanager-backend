/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //identite
    nom: { type: 'string', required: true, },
    prenom: { type: 'string', required: true,   },
    dateNaissance: { type: 'string', required: true,  },
    adhesion: { type: 'string',  },
    role: { type: 'string', defaultsTo: "user" },
    telephone: { type: 'string', required: true, unique: true },
    email: { type: 'string', required: true, },
    motDePasse: { type: 'string', required: true, },


    //adresse
    adresse: { type: 'string', required: true, },
    codePostal: { type: 'string', required: true, },
    ville: { type: 'string', required: true, },

    //photo
    avatarUrl: { type: 'string',  },
    avatarName: { type: 'string',  },
    avatarFd: { type: 'string',  },

    stays: {
      collection: 'stay',
      via: 'usersys'
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  
  },

};

