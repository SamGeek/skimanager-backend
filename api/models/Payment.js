/**
 * Payment.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    //ces deux champs constituent la clé primaire de mon objet payment
    stay: { type: 'string',  required: true,},
    user: { type: 'string', required: true,},

    //les montants
    coutSejour: { type: 'number', defaultsTo : 0 },
    coutTotalSejour: { type: 'number', defaultsTo : 0 },
    montantPayer: { type: 'number', defaultsTo : 0 },//nuyance ce montant integre le total des accomptes, et le solde lors du reglement de la facture
    resteAPayer: { type: 'number', defaultsTo : 0 },
    totalRemise: { type: 'number',defaultsTo : 0 },
    totalSupplement: { type: 'number',defaultsTo : 0 },
    totalAccompte: { type: 'number', defaultsTo : 0},
    hebRestau: { type: 'number', defaultsTo : 0},
    forfaits: { type: 'number', defaultsTo : 0},

    cat1: { type: 'number', defaultsTo : 0},
    cat2: { type: 'number', defaultsTo : 0},
    cat3: { type: 'number', defaultsTo : 0},
    cat4: { type: 'number', defaultsTo : 0},
    cat5: { type: 'number', defaultsTo : 0},
    cat6: { type: 'number', defaultsTo : 0},
    cat7: { type: 'number', defaultsTo : 0},
    catAdulte: { type: 'number', defaultsTo : 0},
    catEnfant: { type: 'number', defaultsTo : 0},

    shortTripsAdultes: { type: 'number', defaultsTo : 0},
    shortTripsEnfants: { type: 'number', defaultsTo : 0},
    otherTripsAdultes: { type: 'number', defaultsTo : 0},
    otherTripsEnfants: { type: 'number', defaultsTo : 0},

    //ici on a tous les attributs pour faire le compte sur la facture etc...
    //hauteSaison
    nuitee: { type: 'number', defaultsTo : 0},
    repasAdulte: { type: 'number', defaultsTo : 0},
    repasEnfant: { type: 'number', defaultsTo : 0},
    //basseSaison
    nuiteeBasseSaison: { type: 'number', defaultsTo : 0},
    repasAdulteBasseSaison: { type: 'number', defaultsTo : 0},
    repasEnfantBasseSaison: { type: 'number', defaultsTo : 0},
 


    //le statut
    estSolder: {  type: 'boolean', defaultsTo: false, },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    //bon la les vraies assos

    //Liste des accomptes
    deposits: {
      collection: 'deposit',
      via: 'payment'
    },

    //Liste des supplements
    extracosts: {
      collection: 'extracost',
      via: 'payment'
    },

    //Liste des remises 
    discounts: {
      collection: 'discount',
      via: 'payment'
    },

  },

};

