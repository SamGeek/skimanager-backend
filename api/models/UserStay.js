/**
 * UserStay.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    //C'est le user stay qui est rajouté au séjour pour une meilleure modélisation

    nom: { type: 'string', required: true, },
    prenom: { type: 'string', required: true,   },
    dateNaissance: { type: 'string', required: true,  },
    adhesion: { type: 'string', required: true,  },


    //le creator cest celui qui crée le ajoute cet utilisateur au séjour
    //ce champ doit etre requis et permets de savoir a quel utilisateur de l'appli chaque user Stay est rataché
    creator: {
      model: 'user',
      required: true
    },

    // Add a reference to Stay
    stay: {
      model: 'stay',
      required: true
    },

    mealsPerDay: {
      collection: 'mealperday',
      via: 'userstay'
    },

    skipasses: {
      collection: 'skipass',
      via: 'userstaypass'
    },
  


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

