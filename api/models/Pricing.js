/**
 * Pricing.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
       //identite
      periode: { type: 'string', required: true,  isIn: ['ete', 'hiver'] },
      categorie: { type: 'string', required: true,  isIn: ['zero_cinq', 'six_neuf', 'dix_douze', 'treize_dixsept', 'dixhuit_vingtneuf', 'dixhuit_soixantequatre', 'soixantecinq_plus'] },
      chaletRepas: { type: 'number', defaultsTo : 0},
      chaletNuitee: { type: 'number', defaultsTo : 0 },
      basseSaisonRepas: { type: 'number', defaultsTo : 0 },
      basseSaisonNuitee: { type: 'number', defaultsTo : 0 },
      adhesionSejour: { type: 'number', defaultsTo : 0 },
      adhesionWeekEnd: { type: 'number', defaultsTo : 0  },

      forfait1jour: { type: 'number', defaultsTo : 0 },
      forfait2jours: { type: 'number', defaultsTo : 0 },
      forfait3jours: { type: 'number', defaultsTo : 0 },
      forfait4jours: { type: 'number', defaultsTo : 0  },
      forfait5jours: { type: 'number', defaultsTo : 0},
      forfait6jours: {type: 'number', defaultsTo : 0 },
      forfait7jours: {type: 'number', defaultsTo : 0 },
      forfait8jours: { type: 'number', defaultsTo : 0},
      forfait9jours: { type: 'number', defaultsTo : 0 },
      forfait10jours: { type: 'number', defaultsTo : 0 },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

