/**
 * WebAppControllerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var moment = require('moment');

module.exports = {


  //cree l'utilisateur, et l'ajoute au séjour
  //ceci nest fait qu'une fois par groupe d'utilisateurs
  createUserAndAddToStay: async function (req, res) {

    var stay = await Stay.findOne({ id: req.allParams().stay, });
    // If there was no matching stay, respond thru the "badCombo" exit.
    if (!stay) {
      return res.notFound();
    }

    //ceci est important parceque le telephone est la  clé primaire
    // donc si un utilisateur existe deja avec ce numéro de télphone on doit refuser
    var users = await User.find({ where: { telephone: req.allParams().telephone, } });
    // If there is a  matching user, respond thru the "badCombo" exit.
    if (users.length > 0) {
      return res.badRequest();
    }

    //depuis la plateforme Web, on doit dabor sauvegarder l'utilisateur 
    var userCreated = await User.create({
      nom: req.allParams().nom,
      prenom: req.allParams().prenom,
      dateNaissance: req.allParams().dateNaissance,
      telephone: req.allParams().telephone,
      email: req.allParams().email,
      motDePasse: req.allParams().motDePasse,
      adresse: req.allParams().adresse,
      codePostal: req.allParams().codePostal,
      ville: req.allParams().ville,
    }).fetch();



    var userStayCreated = await UserStay.create({
      nom: req.allParams().nom,
      prenom: req.allParams().prenom,
      dateNaissance: req.allParams().dateNaissance,
      adhesion: req.allParams().adhesion,
      //ici le creator est lutilisateur nouvellement créé
      creator: userCreated.id,
      stay: stay.id,
    }).fetch();

    //ce serait bien de rajouter les meals per day en meme temps a la création du user
    //ici on doit faire appel au helper feelMeals pour creer les meals et les ajouter au userStayCreated

    var daysBetween = await sails.helpers.getDaysBetween.with({
      startDateP: stay.startDate,
      endDateP: stay.endDate
    })

    var meals = []

    for (let day of daysBetween) {
      //je prends day et je cree un mealPerday pour chaque day
      var meal = {};
      meal.day = day;
      meal.userstay = userStayCreated.id;
      meal.matin = true; meal.midi = true; meal.soir = true;

      if (day === daysBetween[daysBetween.length - 1]) meal.soir = false;


      var mealCreated = await MealPerDay.create(meal).fetch();
      await UserStay.addToCollection(userStayCreated.id, 'mealsPerDay').members(mealCreated.id);

      //dans la vraie vie je dois créer une instance de mealPerDay et l'associer au user
      meals.push(meal);
    }


    //apres faire un service pour modifier les meals per day, dun userStay donné
    await User.addToCollection(userCreated.id, 'stays').members(stay.id);

    // je le recupere a nouveau en populant cette fois ci ses mealsPerDay   
    var userStay = await UserStay.findOne({ id: userStayCreated.id, }).populate('mealsPerDay').populate('skipasses');

    //on peut maintenant créer le paiement pour cette association, user and stay
    //je recherche dabor le payment
    var payments = await Payment.find({ where: { stay: stay.id, user: userCreated.id } });
    var paymentCreated;


    if (payments.length == 0) {
      //si cest vide, on doit creer un payment avec les bonnes infos
      paymentCreated = await Payment.create({
        stay: stay.id,
        user: userCreated.id,
        coutSejour: await sails.helpers.calculateUserCost.with({ stayId: stay.id })
      }).fetch();
      paymentCreated = await sails.helpers.updatePaiementCosts.with({ paymentId: paymentCreated.id })
    } else {
      sails.log('else paiement ', payments)
      paymentCreated = payments[0]
      paymentCreated = await sails.helpers.updatePaiementCosts.with({ paymentId: paymentCreated.id })
    }


    //je veux voir ce qu'il y a  dans le paiement
    return res.ok(paymentCreated)
    // return res.ok(userStay)
  },


  testCostCompute: async function (req, res) {

    var stay = await Stay.findOne({ id: req.allParams().stay, });
    // If there was no matching stay, respond thru the "badCombo" exit.
    if (!stay) {
      return res.notFound();
    }

    // sails.log('séjour',stay);

    var user = await User.findOne({ telephone: req.allParams().telephone, });
    // If there was no matching user, respond thru the "badCombo" exit.
    if (!user) {
      return res.notFound();
    }

    // sails.log('utilisateur',user);

    var cost = await sails.helpers.calculateUserCost.with({
      // userId: user.id,
      stayId: stay.id
    })


    //normalement on doit rajouter un objet payment qui est associé a un user du systeme et a un séjour
    //cela doit se faire, a chaque fois qu'on ajoute un usersatay a un stay, et le montant de ce payement doit etre mis a jour 
    //a chaque modification


    //donc ceci est a faire a deux endroits : le userController addUserToStay et le webAppController, la meme methode
    //normalement un seul si jarrive a creer un helper pour me faire le scénario nominal a faire plutard

    return res.ok(cost)
  },


  //maintenant, je dois faire 3 services pour ajouter chaque chose
  //Discount, ExtraCost et Deposit

  addDiscount: async function (req, res) {
    //je recherche dabor le payment
    var payments = await Payment.find({ where: { stay: req.allParams().stay, user: req.allParams().user } });

    if (payments.length == 0) {
      return res.notFound();
    }

    var payment = payments[0];

    var discountCreated = await Discount.create({
      montant: req.allParams().montant,
      pourcentage: req.allParams().pourcentage,
      explication: req.allParams().explication,
      payment: payment.id
    }).fetch();

    //mettre a jour le payment

    // var nouveauTotalRemise =  payment.totalRemise + discountCreated.montant
    // var updatedPayment =  await Payment.update( {  id: payment.id }).set({
    //   totalRemise: nouveauTotalRemise,
    //   resteAPayer: payment.coutSejour - nouveauTotalRemise- payment.montantPayer
    // }).fetch();  

    //on pourrait le renvoyer en retour du service mais ce serait trop verbose
    var updatedPayment = await sails.helpers.updatePaiementCosts.with({ paymentId: payment.id })

    return res.ok(discountCreated)
  },


  addExtraCost: async function (req, res) {
    //je recherche dabor le payment
    var payments = await Payment.find({ where: { stay: req.allParams().stay, user: req.allParams().user } });

    if (payments.length == 0) {
      return res.notFound();
    }

    var payment = payments[0];

    var extraCostCreated = await ExtraCost.create({
      montant: req.allParams().montant,
      libelle: req.allParams().libelle,
      typeSupplement: req.allParams().typeSupplement,
      payment: payment.id
    }).fetch();

    //mettre a jour le payment

    // var nouveauTotalSupplement =  payment.totalSupplement + extraCostCreated.montant
    // var updatedPayment =  await Payment.update( {  id: payment.id }).set({
    //   totalSupplement: nouveauTotalSupplement,
    //   coutSejour: payment.coutSejour + extraCostCreated.montant,
    //   resteAPayer: payment.coutSejour + extraCostCreated.montant - payment.montantPayer - payment.totalRemise
    // }).fetch();  

    //on pourrait le renvoyer en retour du service mais ce serait trop verbose
    var updatedPayment = await sails.helpers.updatePaiementCosts.with({ paymentId: payment.id })

    return res.ok(extraCostCreated)
  },

  addDeposit: async function (req, res) {
    //je recherche dabor le payment
    sails.log('jaffiche lutilisateur passé en parametres',req.allParams().user )
    var payments = await Payment.find({ where: { stay: req.allParams().stay, user: req.allParams().user } });

    if (payments.length == 0) {
      return res.notFound();
    }

    var payment = payments[0];

    var depositCreated = await Deposit.create({
      montant: req.allParams().montant,
      typePaiement: req.allParams().typePaiement,
      numeroCheque: req.allParams().numeroCheque,
      banque: req.allParams().banque,
      debiteur: req.allParams().debiteur,
      payment: payment.id
    }).fetch();

    // //mettre a jour le payment
    // var nouveauMontantPayer =  payment.montantPayer + depositCreated.montant
    // var updatedPayment =  await Payment.update( {  id: payment.id }).set({
    //   montantPayer: nouveauMontantPayer,
    //   resteAPayer: payment.coutSejour - payment.totalRemise- nouveauMontantPayer,
    //   totalAccompte : payment.totalAccompte + depositCreated.montant
    // }).fetch();  

    //on pourrait le renvoyer en retour du service mais ce serait trop verbose
    var updatedPayment = await sails.helpers.updatePaiementCosts.with({ paymentId: payment.id })

    return res.ok(depositCreated)
  },



  fetchPayment: async function (req, res) {
    //je recherche dabor le payment
    var payments = await Payment.find({ where: { stay: req.allParams().stay, user: req.allParams().user } });

    if (payments.length == 0) {
      return res.notFound();
    }

    var payment = payments[0];

    return res.ok(payment)
  },



  fetchGroupsOfStay: async function (req, res) {
    //je recherche dabor le séjour, il faut populer tout au moins les usersys
    var stay = await Stay.findOne({ id: req.allParams().stay }).populate('usersys');

    //si on ne retrouve pas le séjour, on va renvoyer un notFound
    if (!stay) {
      return res.notFound();
    }

    //maintenant que jai le séjour, je recuperer tous les usersSys du séjour
    var usersys = stay.usersys

    var groups = []

    for (let user of usersys) {
      var group = {}
      //je dois aussi recueprer le paiement associe pour eviter de faire trop de requetes depuis le frontEnd
      var payments = await Payment.find({ where: { stay: req.allParams().stay, user: user.id } });
      var payment = payments[0];

      //on va mettre a jour les couts de ce paiement pour etre sur que tout est pris en compte avant l'étabilissement de la facture
      payment = await sails.helpers.updatePaiementCosts.with({ paymentId: payment.id })


      //je dois egalement faire une requete pour populer le séjour, j'en ai besoin sur la facture
      var stayPayments = await Stay.find({ where: { id: payment.stay } });
      var stayPayment = stayPayments[0]
      payment.stay = stayPayment

      //je dois faire une requete pour reccuperer le user et le mettre en entier dans le paiement
      var userPayments = await User.find({ where: { id: payment.user } });
      var userPayment = userPayments[0]
      payment.user = userPayment

      //ici je dois recuperer tous les userstay qui sont crees par ce usersys
      var userstays = await UserStay.find({ where: { stay: req.allParams().stay, creator: user.id } }).populate('mealsPerDay').populate('skipasses');

      group.payment = payment
      group.users = userstays

      groups.push(group)
    }

    return res.ok(groups)
  },


  mealsStats: async function (req, res) {

    var mealsStats = [];

    //reccuperer un startDate et un endDate
    //on doit avoir un helper qui decoupe tous les jours dans une periode donnée, je pense qu'il existe deja
    var daysBetween = await sails.helpers.getDaysBetween.with({
      startDateP: req.allParams().startDate,
      endDateP: req.allParams().endDate
    })

    for (let day of daysBetween) {
      //recuperer tous les mealsPerDay pour un jour donné par tranches dage
      //je pense qu'il faut un helper pour ca, il doit prendre en parametre le jour en question
      var mealStat = await sails.helpers.generateMealsStats.with({
        day: day
      });
      mealsStats.push(mealStat);
    }


    //on doit former la reponse et la retourne

    return res.ok(mealsStats)
  },


  //il faut maintenant une statistique pour les forfaits de ski aussi

  skiStats: async function (req, res) {

    var skiStats = [];

    //reccuperer un startDate et un endDate
    //on doit avoir un helper qui decoupe tous les jours dans une periode donnée, je pense qu'il existe deja
    var daysBetween = await sails.helpers.getDaysBetween.with({
      startDateP: req.allParams().startDate,
      endDateP: req.allParams().endDate
    })

    for (let day of daysBetween) {
      //recuperer tous les mealsPerDay pour un jour donné par tranches dage
      //je pense qu'il faut un helper pour ca, il doit prendre en parametre le jour en question
      var skiStat = await sails.helpers.generateSkiStats.with({
        day: day
      });
      skiStats.push(skiStat);
    }


    //on doit former la reponse et la retourne

    return res.ok(skiStats)
  },


  //il faut bien gerer la suppression dun userStay, il faut supprimer
  //tous les trucs qui lui sont associes, les skipasses et les mealsPerDay
  //cest capital


  //a la création du paiement je dois lui creer un numéro unique


  //je dois faire ici un service pour supprimer un séjour
  //supprimer un séjour c'est un service costaud, 
  // on doit parcourir tous les userstay pour chacun deux, supprimer tous les skipasses et les mealsPerDay
  //ensuite supprimer le usertay lui meme
  //une fois tout cela fait, on doit supprimer l'objet paiement associé a ce séjour
  //c'est alors que lon peut supprimer le séjour lui meme

  deleteStay: async function (req, res) {
    //verifier apres sil nya pas des remove from collections a faire selon le cas

    //on recupere le séjour avec son ID
    var stay = await Stay.findOne({ id: req.allParams().stay, }).populate('users').populate('usersys');
    // If there was no matching stay, respond thru the "badCombo" exit.
    if (!stay) {
      return res.notFound();
    }

    //maitenant que nous avons le séjour, en question, on recupere tous les userStay qu'il contient
    for (let user of stay.users) {
      //je dois recuperer les mealsPerDay de chaque chaque userStay
      userStay = await UserStay.findOne({ id: user.id, }).populate('mealsPerDay').populate('skipasses');
      //on recupere dabord les skipasses du user, on les supprime tous 
      for (let pass of userStay.skipasses) {
        await Skipass.destroy({ id: pass.id });
      }
      //ensuite, on fait pareil pour les mealsPerDay
      for (let meal of userStay.mealsPerDay) {
        await MealPerDay.destroy({ id: meal.id });
      }
      //maintenant, on peut supprimer l'objet user actuel
      await UserStay.destroy({ id: user.id });
    }

    //maintenant, on supprime le paiement associé a ce séjour
    var payments = []
    var payment = {}
    if (stay.usersys.length > 0) {
      //normalement avant de supprimer les paiements, il faut toutes les autres choses associées 
      //ces autres choses sont tous les deposits/extracosts/discounts associes
      //normalement sails doit permettre de supprimer en profondeur et en cascade des elements pour ne pas créer des orphelins
      payments = await Payment.find({ where: { stay: req.allParams().stay, user: stay.usersys[0].id } });
      payment = payments[0];
      await Payment.destroy({ id: payment.id });
    }


    //eet enfin, on peut supprimer le séjour lui meme
    await Stay.destroy({ id: stay.id });

    return res.ok()
  },

  // on doit faire un servicee pour enregistrer la facture
  // cest au moment de l'enregistrement, qu'on doit génerer le numero de facture
  saveBill: async function (req, res) {
    //ici on va generer le numero de facture
    //CH20200818+ID
    var today = new Date()
    // il faut que je recupere la derniere facture et que je tronque les deux derniers chiffres

    // jai juste a recureprer toutes les factures avant ce soir a 23H
    //vu kon ne peut pas creer des facture pour demain
    var now = moment.utc()//on prends la date d'aujourdhui
    sails.log('printing the date itself',today)
     sails.log('check today end normal',now.endOf('day').toDate())
    // sails.log('check today formatted to millis',now.endOf('day').toDate().format("X"))
    var lastBill = await Bill.find({ where: { day: {'>=': now.startOf('day').toDate(),'<=': now.endOf('day').toDate()}, } })
      .sort('createdAt DESC')
      //cette requete ci ne marche pas il faut que je trouve un meilleur moyen de faire ca

    lastBill = lastBill[lastBill.length-1]

    var billNumber = ''
    var mois = ("0" + (today.getUTCMonth() + 1)).toString().slice(-2)
    var jour = ("0" + today.getUTCDate()).toString().slice(-2)
    // sails.log('jaffiche la date pour etre sur avec le slice', jour  +'/'+mois+'/'+today.getUTCFullYear())
    // sails.log('jaffiche la date pour etre sur',parts[2]  +'/'+mois+'/'+jour)
    // sails.log('jaffiche le truc optimal', today.getUTCDate()  +'/'+(today.getUTCMonth() + 1)+'/'+today.getUTCFullYear())

    sails.log('last bill found', lastBill)

    

    if (lastBill) {
      //sil y a un last bill on recupere son numero de facture et on fait ce qu'il faut avec
      //ne surtout pas oublier dincrémenter 
      var ordre = parseInt(lastBill.billNumber.substring(lastBill.billNumber.length - 2)) + 1 //il faudra tester cette troncarture
      // il y a un bugg sur la gestion de cet increment a fixer plutard
      ordre = ("0" + ordre).toString().slice(-2)
      billNumber = 'CH' + today.getUTCFullYear() + '' + mois + '' + jour + '' + ordre

    } else {
      // sil ny a pas de last bill donc cest la premiere facture de la journée, on génere un nouveau numero
      billNumber = 'CH' + today.getUTCFullYear() + '' + mois + '' + jour + '01'
    }

    //---------------------------------------------------

    //ceci est bon on peut passer a létape dapres
    //on recuperer la meme facture si elle existe si non, on la cree 
    var payments = await Payment.find({ where: { id: req.allParams().paymentId, } });
    var payment = payments[0];

    //on doit aller chercher une facture avec ce paiement

    delete payment.id;
    var facture = payment
    facture.billNumber = billNumber
    facture.day = today

    //on rajoute les prix suceptibles de changer en fonction du temps
    facture.chaletNuitee  = req.allParams().chaletNuitee
    facture.chaletRepasAdulte  = req.allParams().chaletRepasAdulte
    facture.chaletRepasEnfant  = req.allParams().chaletRepasEnfant
    facture.basseSaisonNuitee  = req.allParams().basseSaisonNuitee
    facture.basseSaisonRepasAdulte  = req.allParams().basseSaisonRepasAdulte
    facture.basseSaisonRepasEnfant  = req.allParams().basseSaisonRepasEnfant

    // chaletNuitee : pricingsHiver[6].chaletNuitee,
    // chaletRepasAdulte : pricingsHiver[6].chaletRepas ,
    // chaletRepasEnfant :pricingsHiver[2].chaletRepas,
    // basseSaisonNuitee :pricingsHiver[6].basseSaisonNuitee,
    // basseSaisonRepasAdulte : pricingsHiver[6].basseSaisonRepas,
    // basseSaisonRepasEnfant : pricingsHiver[6].basseSaisonRepas


    //faire ce traitement la plutard

    var factureCreated = await Bill.create(facture).fetch();


    //---------------------------------------------------

    // if (payement) {
    //   //si le paiement existait, on le modifie
    //   updatedStay = await Payment.update({ id: payment.id }).set(
    //     facture
    //   ).fetch();

    // } else {
    //   //si le paiement n'existait pas, on le crée

    // }

    //je teste cette affaire la juska etre sur que la generation de numero fonctionne bien et ensuite, je le mets en RTCStatsProvider


    //si une facture existe deja, on mets a jour les infos 
    //les infos en question ce sont tous les montant mais le numero de facture peut changer mais une seule facture doit exister
    return res.ok(factureCreated)
  },


  // il me faut un modele pour enregistrer la facture 
  // il faut aussi un numero de facture qui sera généré
  // il faut générer la comptabilité
  // et tout retester un peu pour fixer les choses
  // il faut faire un login et faire la gestion des rôles
  // je dois changer toutes les dates du système et les mettre au bon format en m'assurant qu'il nya pas deffets de bord
  // je dois faire les corrections sur le mobile mais pas tout de suite 


  // il faut que je rajoute du déploiement continu sur le frontend ou trouver un moyen de le deployer efficacement





  // //je dois tester le parseDate isolément
  // testParsing: async function (req, res) {
  //   var startDate = await sails.helpers.parseDate(req.allParams().date);
  //   return res.ok(startDate)
  // }



  //jai besoin d'un service qui donne tous les details financiers, pour un groupe donné
  fetchFinancialDetails: async function (req, res) {
    var finances= {}
    payments = await Payment.find({ where: { stay: req.allParams().stay, user: req.allParams().user } });
    payment = payments[0];

    //a chaque fois, on cherche toutes les entitéss voulues, associees a ce payment precis
    deposits = await Deposit.find({ where: { payment: payment.id } });
    discounts = await Discount.find({ where: { payment: payment.id } });
    extracosts = await ExtraCost.find({ where: { payment: payment.id } });

    finances.deposits = deposits
    finances.discounts = discounts
    finances.extracosts = extracosts

    return res.ok(finances)
  },





};

