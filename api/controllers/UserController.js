/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const fs = require('fs')
const path = require('path')
Promise = require('bluebird');


module.exports = {

    //service de recherche d'un stay avec son code
    /** A coder par moi meme */

    findStay: async function (req, res) {

        var stay = await Stay.findOne({ code: req.code, });
        // If there was no matching stay, respond thru the "badCombo" exit.
        if(!stay) {
            return res.notFound();
        }
    
        return res.success(stay)
      },

    //servie pour ajouter un user a un stay
    /** disponible avec le blueprint */

    //service d'inscription
    /** disponible avec le blueprint */

    //service de connexion
    // /user/login


    //service de resetPassword
    /** A coder par moi meme */



    //ajout des meals per day a un utilisateur
    /** A coder par moi meme */

    addMealsPerDay: async function (req, res) {

        var stay = await Stay.findOne({ code: req.code, });
    
        // If there was no matching stay, respond thru the "badCombo" exit.
        if(!stay) {
            return res.notFound();
        }
    
        return res.success(stay)
      },


    //ajout des skipass a un utilisateur
    /** disponible avec le blueprint */



    //Calcul du cout du séjour
    /** A coder par moi meme */



    //liste des séjours
    /** disponible avec le blueprint */




    //services de modification du profil utilisateur
    /** disponible avec le blueprint */



    //////////////////////////// Gestion de la partie organisateur /////////////////////////////////

    RandomCode: function(length) {
        var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    },


    //creation d'un stay (CRUD)
    /** A coder par moi meme a cause :
     * - de la génération du code de séjour
     * - de l'upload de la photo liée au séjour */


    addStay: async function (req, res) {

        var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        var code = '';

       // sails.log('requete 1 : ',req.allParams().data)
    // sails.log('requete 1 : '+req.data.traderPhoneNumber)
    // const data = JSON.parse(req.allParams().data)

    
        for (var i = 10; i > 0; --i) code += chars[Math.floor(Math.random() * chars.length)];
        
        var stayRecord = await Stay.findOne({
            code: code,
        });


        while(stayRecord  !== undefined) {
            code = ''
            for (var i = 10; i > 0; --i) code += chars[Math.floor(Math.random() * chars.length)];
            stayRecord = await Stay.findOne({
                code: code,
            });
        }
    
        var stayCreated = await Stay.create({
            name: req.allParams().name,
            code: code,
            startDate: req.allParams().startDate,
            endDate: req.allParams().endDate,
            stayType: req.allParams().stayType,
            totalPlaces: req.allParams().totalPlaces,
            booker: req.allParams().booker,
        }).fetch();

        // const uploadDir = path.resolve(
        //     sails.config.appPath,
        //     sails.config.globals.uploadDirs['default']
        // );

        // if (!fs.existsSync(uploadDir)){
        //     fs.mkdirSync(uploadDir)
        // }

        // req.file('avatar').upload({
        // dirname: uploadDir,
        // // don't allow the total upload size to exceed ~10MB
        // maxBytes: 10000000
        // },async function whenDone(err, uploadedFiles) {
        // if (err) {
        //     return res.serverError(err);
        // }

        // // If no files were uploaded, respond with an error.
        // if (uploadedFiles.length === 0){
        //     return res.badRequest('No file was uploaded');
        // }

        // // Get the base URL for our deployed application from our custom config
        // // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
        // var baseUrl = sails.config.custom.baseUrl;

        // // Save the "fd" and the url where the avatar for a user can be accessed
        // var updatedStay =  await Stay.update( {  id: stayCreated.id }).set({
        //     // Generate a unique URL where the avatar can be downloaded.
        //     avatarUrl: require('util').format('%s/stay/avatar/%s', baseUrl, stayCreated.id),
        //     avatarName: req.file('avatar')._files[0].stream.filename,
        //     // Grab the first file and use it's `fd` (file descriptor)
        //     avatarFd: uploadedFiles[0].fd
        // }).fetch();

        
        return res.ok(stayCreated);

        // });
    },

  
  fetchStayById: async function (req, res){

    var stays =  await Stay.find({booker: req.allParams().userID,});


    var staysUpdated = [];

    for (let stay of stays) {
      //je prends chaque stay, je recupere depuis la base en populant ses users
      stay =  await Stay.findOne({ id: stay.id, }).populate('users');
      staysUpdated.push(stay);
    }

   return res.ok(staysUpdated)



    return res.ok(stays)
  },



  fetchStaysOfUser: async function (req, res){

    var user =  await User.findOne({ id: req.allParams().userID, }).populate('stays');

    var staysUpdated = [];

    for (let stay of user.stays) {
      //je prends chaque stay, je recupere depuis la base en populant ses users
      stay =  await Stay.findOne({ id: stay.id, }).populate('users');

      var usersStay = [];
      for (let user of stay.users) {
        //je dois recuperer les mealsPerDay de chaque chaque userStay
        userStay =  await UserStay.findOne({ id: user.id, }).populate('mealsPerDay').populate('skipasses');
        usersStay.push(userStay);
      }
      stay.users = usersStay;
      staysUpdated.push(stay);
    }

   return res.ok(staysUpdated)
  },

   
  fetchStaysOfUserNew: async function (req, res){

    var user = await User.findOne({ id: req.allParams().userID, });
    var stay = await Stay.findOne({ id: req.allParams().stay, });

    await User.addToCollection(user.id, 'stays').members(stay.id);

    //   // Save the "fd" and the url where the avatar for a user can be accessed
    //   var updatedTrade =  await User.update( {  id: user.id }).set({
    //     code:code,
    //     // Generate a unique URL where the avatar can be downloaded.
    //     avatarUrl: require('util').format('%s/trade/avatar/%s', baseUrl, tradeCreated.id),
    //     avatarName: req.file('avatar')._files[0].stream.filename,

    //     // Grab the first file and use it's `fd` (file descriptor)
    //     avatarFd: uploadedFiles[0].fd
    //   }).fetch();

    // var user = await User.findOne({ creator: req.allParams().stay, }).populate(stays, {
    //     where: {id : req.allParams().stay}
    // });

    var userUpdated = await User.findOne({ id: req.allParams().userID, });

    return res.ok(userUpdated)
  },

  addSkiPassToUserStay: async function (req, res){

    //var userStay = await UserStay.findOne({ id: req.allParams().userId, });
    var userStay =  await UserStay.find({ where: { id: req.allParams().userId, } });
    userStay = userStay[0]

    // si on ne trouve aucun userStay avec le bon ID on doit retourner un notFound
    if(!userStay) {
        return res.notFound();
    }


    sails.log('affichons le passName envoyé au service', req.allParams().passName)

    //si on trouve le bon user stay on doit creer le forfait de ski et lajouter au userStay
    //on doit faire un petit helper qui retourne la bonne couleur en fonction du type de forfait choisi
    var skiPassCreated = await SkiPass.create({
      passName : req.allParams().passName,
      passColor : await sails.helpers.getSkiPassColor.with({passName:req.allParams().passName})
    }).fetch();

    //ajout du SkiPass au userStay
    await UserStay.addToCollection(userStay.id, 'skipasses').members(skiPassCreated.id);


    //on doit populer la liste des forfaits et la retourner au client
    var skipass = await SkiPass.findOne({ id: skiPassCreated.id, });
    //ca cest le UserStay populé avec la liste des skipass
    //var userStay = await UserStay.findOne({ id: req.allParams().userId, }).populate('skipasses');

    return res.ok(skipass)
  },
 
  addUserToStay: async function (req, res){


    //mettre a jour le nombre de places dispo
    //pour ce faire recalculer le nombre de userstay pour ce séjour précis, le mettre a jour et si limite atteinte quitter
    //besoin d'un petit helper pour ca

    var stay = await Stay.findOne({ id: req.allParams().stay, });
    // If there was no matching stay, respond thru the "badCombo" exit.
    if(!stay) {
        return res.notFound();
    }

    var userStayCreated = await UserStay.create({
        nom: req.allParams().nom,
        prenom: req.allParams().prenom,
        dateNaissance: req.allParams().dateNaissance,
        adhesion: req.allParams().adhesion,
        creator: req.allParams().creator,
        stay: stay.id,
    }).fetch();

    //ce serait bien de rajouter les meals per day en meme temps a la création du user
    //ici on doit faire appel au helper feelMeals pour creer les meals et les ajouter au userStayCreated

    var daysBetween = await sails.helpers.getDaysBetween.with({
      startDateP:stay.startDate,
      endDateP: stay.endDate
    })

    var meals = []

    for (let day of daysBetween) {
      //je prends day et je cree un mealPerday pour chaque day
      var meal = {};
      meal.day =  day;
      meal.userstay =  userStayCreated.id;
      meal.matin = true;meal.midi = true;meal.soir = true;

      if(day === daysBetween[daysBetween.length-1]) meal.soir = false;


      var mealCreated = await MealPerDay.create(meal).fetch();
      await UserStay.addToCollection(userStayCreated.id, 'mealsPerDay').members(mealCreated.id);

      //dans la vraie vie je dois créer une instance de mealPerDay et l'associer au user
      meals.push(meal);
    }


    //apres faire un service pour modifier les meals per day, dun userStay donné

    var user = await User.findOne({ id: req.allParams().creator, });
    await User.addToCollection(user.id, 'stays').members(stay.id);

    // je le recupere a nouveau en populant cette fois ci ses mealsPerDay   
    var userStay =  await UserStay.findOne({ id: userStayCreated.id, }).populate('mealsPerDay').populate('skipasses');


    //deux choses supplémentaires, restent a faire ici
    //on doit creer  un payment seulement si le bon payment nexiste pas 
    //et en meme temps, mettre a jour ce payment, avec le cout du séjour, calculé avec mon magnifique helper

     //je recherche dabor le payment
     var payments = await Payment.find({where: {stay: stay.id, user: user.id }});

     var paymentCreated;

    if(payments.length == 0) {
      //si cest vide, on doit creer un payment avec les bonnes infos
      paymentCreated = await Payment.create({
        stay: stay.id,
        user: user.id
      }).fetch();
      await sails.helpers.updatePaiementCosts.with({paymentId: paymentCreated.id })
    }else{
      paymentCreated = payments[0]
      await sails.helpers.updatePaiementCosts.with({paymentId: paymentCreated.id })
    }


    return res.ok(userStay)
  },


   fillMeals: async function(req, res) {
    //on doit faire une nouvelle gestion de fillMeal pour d'adapter a la liste des stays
    var daysBetween = await sails.helpers.getDaysBetween.with({
      startDateP:req.allParams().startDateP,
      endDateP: req.allParams().endDateP
    })

    var meals = []

    for (let day of daysBetween) {
      //je prends day et je cree un mealPerday pour chaque day
      var meal = {};
      meal.day =  day;
      meal.matin = true;meal.midi = true;meal.soir = true;

      if(day === daysBetween[daysBetween.length-1]) meal.soir = false;
      //dans la vraie vie je dois créer une instance de mealPerDay et l'associer au user
      meals.push(meal);
    }


    return res.ok(meals);
  },

  getDaysBetween: async function(req, res) {

    //le tableau qui va contenir la liste des jours dans lintervalle fourni en paramètre
    var days = [] 
    var startDate = await sails.helpers.parseDate(req.allParams().startDateP);
    var tomorrow = await sails.helpers.parseDate(req.allParams().startDateP);
    var endDate = await sails.helpers.parseDate(req.allParams().endDateP);
    

    var timeDiff = Math.abs(startDate.getTime() - endDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

    for (let i = 0; i <= diffDays; i++) {
      var innerDate =  new Date(tomorrow)
      days.push(innerDate);
      innerDate = new Date(tomorrow.setDate(tomorrow.getDate() + 1));
    }
    return res.ok(days);
  },

  //cloturer un stay
  /** A coder par moi meme */


  //gestion de tout ce qui est Pricing
  fetchPricingByPeriod: async function (req, res){
    var pricings = await Pricing.find({ periode: req.allParams().periode, });
    return res.ok(pricings)
  },


  updateMeals: async function(req, res) {

    //pour recuperer le bon payment, il me faut le creator et le stay
    var payments = await Payment.find({where: {stay: req.allParams().stay, user: req.allParams().user }});
    sails.log("jaffiche le paiement trouvé")
    //normalement si on ne le trouve pas on doit le creer et recalculer le cout du séjour
    if(payments.length == 0) {
      return res.notFound();
    }
 
    var payment = payments[0];
    //on a besoin de ce paiement a la fin de la mise a jour des meals

    var meals = req.allParams().meals
    var mealsResponse = []

    for (let meal of meals) {
      var mealUpdated  = await MealPerDay.update( {  id: meal.id }).set({
        matin: meal.matin,
        midi: meal.midi,
        soir: meal.soir
      }).fetch();//ce truc retourne un tableau penser a ne recuperer que lelement a lindice 0 si doit on exploiter cette reponse  
      mealsResponse.push(mealUpdated);
    }

    //apres avoir mis a jour les mealsPerday, on mets a jour le cout du séjour ainsi que tous les autres couts pour le paiment en question
    await sails.helpers.updatePaiementCosts.with({paymentId: payment.id })

    var response = {meals:mealsResponse}


    return res.ok(response);
  },

};

