module.exports = {


  friendlyName: 'Generate ski stats',
  description: '',


  inputs: {
    day: {
      type: 'string',
      example: '',
      description: 'the day we want to get the stat for',
      required: true
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


 
  fn: async function (inputs, exits) {
    //on essaie de recuperer la data passée en parametres, et de recuperer tous les mealsPerDay qui ont la meme date 
    var now = moment.utc(inputs.day)
    var meals =  await Skipass.find({  where: { 
       //day: inputs.day 
       day: {'<=': now.endOf('day').toDate(),'>=': now.startOf('day').toDate()},
      } })// JJ/MM/YYYY
    //ici on doit donner deux params le debut de la journee et la fin de la journée pour
    //que on puisse recuperer tous les repas dans la dite journée

    
    // now.toString()
    // now.startOf('day').toString()
    // now.endOf('day').toString()

    sails.log('meals founds',meals);

    //on initialise lobjet quon va retourner ensuite
    var stat = {};

    var dejeunerAdulte =0;
    var dejeunerEnfant =0;
    var dejeunerPetitEnfant =0;

    var dinerAdulte =0;
    var dinerEnfant =0;
    var dinerPetitEnfant =0;

    var nuiteeAdulte =0;
    var nuiteeEnfant =0;
    var nuiteePetitEnfant =0;

    //maintenant que jai tous les meals pour un jour donné, je fais le compte par catégorie
    for(const meal of meals){

      //je fais ceci pour les meals orphelins
      // mais impérativement penser a bien les nettoyer a la suppresion de l'utilisateur
      if (meal.userstay === null) continue;

      // on a besoin de fetch le userstay du meal pour pouvoir connaitre la categorie dans laquelle il est 
      //je fais un findOne pour voir si ca va marcher ou il va brailler coe dab
      var userStayList = await UserStay.find( {where: {id: meal.userstay}});
      var userStay = userStayList[0];

      //ensuite, on passe son birthDate au helper qui nous permettais de calculer lage pour kil le calcule et en fonction
      //du resultat, on increment la bonne variable
      var ageCategory =  await sails.helpers.calculateUserAgeCategory.with({birthDate: userStay.dateNaissance});

      if(ageCategory === 'treize_dixsept' || ageCategory === 'dixhuit_soixantequatre' || ageCategory === 'soixantecinq_plus') { 
        // la il est adulte
        if(meal.matin) dejeunerAdulte +=1;
        if(meal.midi) dinerAdulte +=1;
        if(meal.soir) nuiteeAdulte +=1;

      } else if (ageCategory === 'six_neuf' || ageCategory === 'dix_douze'){
        //la il est enfant
        if(meal.matin) dejeunerEnfant +=1;
        if(meal.midi) dinerEnfant +=1;
        if(meal.soir) nuiteeEnfant +=1;

      } else{
        //la, il est petit enfant
        if(meal.matin) dejeunerPetitEnfant +=1;
        if(meal.midi) dinerPetitEnfant +=1;
        if(meal.soir) nuiteePetitEnfant +=1;

      }

    }


    //une fois que tous les compteurs sont mis ajour, on retourne une ligne de statistique

    var day = new Date(inputs.day)
    //il faut formatter la date a renvoyer
    stat.day = day.getDate()+'/'+(day.getMonth()+1)+'/'+day.getFullYear()
    
    stat.dejeunerAdulte = dejeunerAdulte;
    stat.dejeunerEnfant = dejeunerEnfant;
    stat.dejeunerPetitEnfant = dejeunerPetitEnfant;

    stat.dinerAdulte = dinerAdulte;
    stat.dinerEnfant = dinerEnfant;
    stat.dinerPetitEnfant = dinerPetitEnfant;

    stat.nuiteeAdulte =  nuiteeAdulte;
    stat.nuiteeEnfant = nuiteeEnfant;
    stat.nuiteePetitEnfant = nuiteePetitEnfant;


    return exits.success(stat)

  }



};

