module.exports = {


  friendlyName: 'Update paiement costs',


  description: '',


  inputs: {
    paymentId: {
      type: 'string',
      example: '',
      description: 'the id of the payment',
      required: true
    },
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {

    // on reccupere le payment avec son ID
    var payments = await Payment.find({ where: { id: inputs.paymentId } });
    var paymentFirst = payments[0];

    //recalculer la bonne valeur du cout du sejour avant de continuer
    await Payment.update({ id: inputs.paymentId }).set({
      coutSejour: await sails.helpers.calculateUserCost.with({ stayId: paymentFirst.stay })
    }).fetch();

    // on reccupere le payment avec son ID
    var payments = await Payment.find({ where: { id: inputs.paymentId } }).populate('extracosts').populate('deposits').populate('discounts');
    var payment = payments[0];

    var extracosts = payment.extracosts;
    var deposits = payment.deposits;
    var discounts = payment.discounts;

    //On mets ajour les remises
    var totalRemiseCalculer = 0;
    for (const discount of discounts) {
      totalRemiseCalculer += discount.montant
    }
    await Payment.update({ id: payment.id }).set({
      totalRemise: totalRemiseCalculer
    }).fetch();


    //les suppléments
    var totalSupplementCalculer = 0;
    for (const extracost of extracosts) {
      totalSupplementCalculer += extracost.montant
    }
    var paymentDefaults = await Payment.update({ id: payment.id }).set({
      totalSupplement: totalSupplementCalculer
    }).fetch();
    var paymentDefault = paymentDefaults[0]

    //les accomptes
    var totalDepositCalculer = 0;
    for (const deposit of deposits) {
      totalDepositCalculer += deposit.montant
    }

    //essayons dafficher un peu les differents trucs
    sails.log('cout sejour ', paymentDefault.coutSejour)
    sails.log('total supplement  ', paymentDefault.totalSupplement)
    sails.log('le calcul ', paymentDefault.coutSejour + paymentDefault.totalSupplement)

    //autre fois on metais a jour le paiement ici, mais le faire maintenant plus bas

    //cest selon moi le meilleur endroit pour éditer les nouveaux couts notemment
    //le total repas et nuittes 
    //le total pour les forfaits de ski

    var stays = await Stay.find({ where: { id: payment.stay } }).populate('users').populate('usersys');
    var usersStay = stays[0].users
    var hebRestau = 0
    var forfaits = 0
    var cat1 = 0
    var cat2 = 0
    var cat3 = 0
    var cat4 = 0
    var cat5 = 0
    var cat6 = 0
    var cat7 = 0
    var catAdulte = 0
    var catEnfant = 0

    sails.log('je veux voir la liste des users stay', usersStay)

    //on a besoin de compter les courts sejours et les sejours normaux
    var shortTripsAdultes = 0
    var shortTripsEnfants = 0
    var otherTripsAdultes = 0
    var otherTripsEnfants = 0


    //il faut compter differents elements
    var nuitee = 0
    var repasAdulte = 0
    var repasEnfant = 0
    //il faut aussi des compteurs pour la basse saison //basseSaison
    var nuiteeBasseSaison = 0
    var repasAdulteBasseSaison = 0
    var repasEnfantBasseSaison = 0


    //il me faut un helper pour calculer la basse saison...

    //la je dois faire un joli for pour les parcourir tous et popupler les bon champs
    for (const element of usersStay) {

      sails.log('je veux afficher chaque element pour voir ce quil contient', element)

      //je fais un findOne pour voir si ca va marcher ou il va brailler coe dab
      var userStayList = await UserStay.find({ where: { id: element.id } }).populate('mealsPerDay').populate('skipasses');
      var userStay = userStayList[0];
      var mealsPerDay = userStay.mealsPerDay;
      var skipasses = userStay.skipasses;

      sails.log('jafficher chaque userstay', userStay)

      //je dois faire un petit helper qui permet de calculer l'age de kkun et de retourner la bonne categorie dans laquelle il se trouve
      //ce helper doit prendre pour seul paramètre la date de Naissance du userStay
      var categoriePricing = await sails.helpers.calculateUserAgeCategory.with({ birthDate: userStay.dateNaissance });

      sails.log('jaffiche la categorie de chaque userstay', categoriePricing)
      //on peut utiliser cette catégorie pour calculer le nombre d'adultes et d'enfants dans le séjour
      //A faire....

      //ici on va compter les courts séjours et les autres séjours
      if (categoriePricing === 'zero_cinq' || categoriePricing === 'six_neuf' || categoriePricing === 'dix_douze'
        || categoriePricing === 'treize_dixsept') { // ici on est enfant
        if (element.adhesion === 'Adhérer pour un w-end') shortTripsEnfants += 1
        else otherTripsEnfants += 1

      } else {//ici on est adulte
        if (element.adhesion === 'Adhérer pour un w-end') shortTripsAdultes += 1
        else otherTripsAdultes += 1
      }

      if (categoriePricing === 'zero_cinq') { cat1 += 1; catEnfant += 1 }
      else if (categoriePricing === 'six_neuf') { cat2 += 1; catEnfant += 1 }
      else if (categoriePricing === 'dix_douze') { cat3 += 1; catEnfant += 1 }
      else if (categoriePricing === 'treize_dixsept') { cat4 += 1; catEnfant += 1 }
      else if (categoriePricing === 'dixhuit_soixantequatre') { cat6 += 1; catAdulte += 1 }
      else if (categoriePricing === 'soixantecinq_plus') { cat7 += 1; catAdulte += 1 }

      var periodePricing = stays[0].stayType === 'pension_complete' ? 'hiver' : 'ete'
      //en supposant ici que les deux paramètres sont renseignes, je recuperer le bon pricing
      var pricings = await Pricing.find({ where: { periode: periodePricing, categorie: categoriePricing } });
      var pricing = pricings[0];
      //en supposant ici que nous avons le bon pricing, on va caculer le bon cout 
      //maintenant on doit parcourir les mealsPerDay et les skipasses

      try {
        //maintenant on mets ajour ce montant en parcourant les skipass
        for (const pass of skipasses) {
          var skiPassKey = await sails.helpers.getSkipassKey.with({ passName: pass.passName }); //get the appropraite key for the skipass, lui donner le parametre pass.passName
          //faire ce sous total dans un champ et le mettre a jour
          forfaits += pricing[skiPassKey]
        }

        //maintenant on mets a jour ce montant en parcourant les mealsPerDay
        for (const meal of mealsPerDay) {
          //ici normalement sur le meal il devrait y avoir un attribut qui nous dis si cest en basse saison ou pas
          //on verifie avec lattribut basseSaison pour faire les calculs

          if (meal.basseSaison) {
            //si cest la basse saison n on fait ceci
            stayCost += meal.matin ? pricing['basseSaisonRepas'] : 0
            stayCost += meal.midi ? pricing['basseSaisonRepas'] : 0
            stayCost += meal.soir ? pricing['basseSaisonNuitee'] : 0
            if (meal.soir) nuiteeBasseSaison += 1

            if (categoriePricing === 'zero_cinq' || categoriePricing === 'six_neuf' || categoriePricing === 'dix_douze'
              || categoriePricing === 'treize_dixsept') { // ici on est enfant

              if (meal.matin) repasEnfantBasseSaison += 1
              else if (meal.midi) repasEnfantBasseSaison += 1

            } else {//ici on est adulte
              if (meal.matin) repasAdulteBasseSaison += 1
              else if (meal.midi) repasAdulteBasseSaison += 1
            }

          } else {
            //si ce nest pas en basse saison, on fait ceci  //penser a faire un helper pour le calcul de la basse et de la haute saison
            //faire ce sous total dans un champ et le mettre a jour
            hebRestau += meal.matin ? pricing['chaletRepas'] : 0
            hebRestau += meal.midi ? pricing['chaletRepas'] : 0
            hebRestau += meal.soir ? pricing['chaletNuitee'] : 0

            // var nuitee = 0
            // var repasAdulte = 0
            // var repasEnfant = 0

            if (meal.soir) nuitee += 1

            if (categoriePricing === 'zero_cinq' || categoriePricing === 'six_neuf' || categoriePricing === 'dix_douze'
              || categoriePricing === 'treize_dixsept') { // ici on est enfant

              if (meal.matin) repasEnfant += 1
              else if (meal.midi) repasEnfant += 1

            } else {//ici on est adulte
              if (meal.matin) repasAdulte += 1
              else if (meal.midi) repasAdulte += 1
            }

          }




        }

      } catch (err) {
        sails.log('erreur lors du traitement', err)
      }

    }
    //nous avons nos nouveaux prix, on les mets a jour dans le paiement

    //aussi tous les compteurs sur le séjour doivent etre mis a jour



    //maintenant que tous les differents montants sont calculés, on peut mettre a jour les differents champs avec les bonnes valeurs
    var updatedPayments = await Payment.update({ id: payment.id }).set({
      totalAccompte: totalDepositCalculer,
      montantPayer: totalDepositCalculer,
      coutTotalSejour: paymentDefault.coutSejour + paymentDefault.totalSupplement,
      hebRestau: parseFloat(parseFloat(hebRestau).toFixed(2)),
      forfaits: parseFloat(parseFloat(forfaits).toFixed(2)),
      cat1: cat1,
      cat2: cat2,
      cat3: cat3,
      cat4: cat4,
      cat5: cat5,
      cat6: cat6,
      cat7: cat7,
      catAdulte: catAdulte,
      catEnfant: catEnfant,
      //ici, on rajoute, tout ce qui est compte des enfants et adultes qui seront affiches sur la facture
      shortTripsAdultes: shortTripsAdultes,
      shortTripsEnfants: shortTripsEnfants,
      otherTripsAdultes: otherTripsAdultes,
      otherTripsEnfants: otherTripsEnfants,

      nuitee: nuitee,
      repasAdulte: repasAdulte,
      repasEnfant: repasEnfant,
      nuiteeBasseSaison: nuiteeBasseSaison,
      repasAdulteBasseSaison: repasAdulteBasseSaison,
      repasEnfantBasseSaison: repasEnfantBasseSaison,

      //il reste resteAPayer et montantPayer mais le champ montantPayer, je vais le gerer apres
      resteAPayer: paymentDefault.coutSejour + paymentDefault.totalSupplement - paymentDefault.totalRemise - totalDepositCalculer

    }).fetch();

    //a la fin on retourne le payment mis a jour
    var updatedPayment = updatedPayments[0]

    return exits.success(updatedPayment);

  }


};

