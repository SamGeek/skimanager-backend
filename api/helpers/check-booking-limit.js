module.exports = {


  friendlyName: 'Check booking limit',


  description: '',


  inputs: {
    stay: {
      type: 'string',
      example: '',
      description: 'the stay to compute',
      required: true
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var stays = await Stay.find({where: {id: inputs.stay}}).populate('users').populate('usersys');
    var stay =  stays[0];
  }


};

