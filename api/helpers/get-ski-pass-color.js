module.exports = {


  friendlyName: 'Get ski pass color',


  description: '',


  inputs: {
    passName: {
      type: 'string',
      example: 'one_day',
      description: 'The name of the pass',
      required: true
    },
  },


  exits: {

    success: {
      outputFriendlyName: 'Ski pass color',
    },

  },


  

  fn: async function(inputs,exits) {
    //la variable qui va contenir la bonne couleur en fonction du pass de ski choisit
    var color ;
    if(inputs.passName === "one_day") color = Number('0xFFF2994A')
    if(inputs.passName === "two_days") color = Number('0xFF219653')
    if(inputs.passName === "three_days") color = Number('0xFF56CCF2')
    if(inputs.passName === "four_days") color = Number('0xFFF2C94C')
    if(inputs.passName === "five_days") color = Number('0xFF9B51E0')
    if(inputs.passName === "six_days") color = Number('0xFF828282')
    if(inputs.passName === "seven_days") color = Number('0xFF83882')
    if(inputs.passName === "eight_days") color = Number('0xFF900082')
    if(inputs.passName === "nine_days") color = Number('0xFF703000')
    if(inputs.passName === "ten_days") color = Number('0xFF008282')

    return exits.success(color);
  },


};

