module.exports = {


  friendlyName: 'Calculate user age category',


  description: '',


  inputs: {
    birthDate: {
      type: 'string',
      example: '',
      description: 'the birthday of the userStay',
      required: true
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    
    //pour commencer, je dois fabriquer un objet de type date avec le parametre passe
    var birthday =  new Date(inputs.birthDate);
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    var age  =  Math.abs(ageDate.getUTCFullYear() - 1970);

    sails.log('lage calculé vaut', age)

    var ageCategory = '';
    //en fonction de lage ainsi calulé, je dois retourner la bonne chaine
    if(age<=5) ageCategory = 'zero_cinq'
    else if(age<=9)  ageCategory = 'six_neuf'
    else if(age<=12)  ageCategory = 'dix_douze'
    else if(age<=17)  ageCategory = 'treize_dixsept'
    else if(age<=64)  ageCategory = 'dixhuit_soixantequatre'
    else   ageCategory = 'soixantecinq_plus'

    return exits.success(ageCategory)

  }


};

