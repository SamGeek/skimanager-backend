module.exports = {


  friendlyName: 'Get skipass key',


  description: '',


  inputs: {
    passName: {
      type: 'string',
      example: '',
      description: 'the birthday of the userStay',
      required: true
    },
  },


  exits: {

    success: {
      outputFriendlyName: 'Skipass key',
    },

  },


  fn: async function (inputs) {

    // Get skipass key.
    var skipassKey='';
    var passName =  inputs.passName;
    if(passName==="one_day") skipassKey ='forfait1jour'
    else if (passName==="two_days")  skipassKey ='forfait2jours'
    else if (passName==="three_days")  skipassKey ='forfait3jours'
    else if (passName==="four_days")  skipassKey ='forfait4jours'
    else if (passName==="five_days")  skipassKey ='forfait5jours'
    else if (passName==="six_days")  skipassKey ='forfait6jours'
    else if (passName==="seven_days")  skipassKey ='forfait7jours'
    else if (passName==="eight_days")  skipassKey ='forfait8jours'
    else if (passName==="nine_days")  skipassKey ='forfait9jours'
    else if (passName==="ten_days")  skipassKey ='forfait10jours'

    // Send back the result through the success exit.
    return skipassKey;

  }


};

