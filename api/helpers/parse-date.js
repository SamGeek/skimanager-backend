module.exports = {

  friendlyName: 'String to date formatter',


  description: 'Return a real date parsed from a String',


  inputs: {

    date: {
      type: 'string',
      example: 'Ami',
      description: 'The string date to parse',
      required: true
    }

  },

  exits: {

    success: {
      description: 'All done.',
    },

  },

  fn: async function (inputs, exits) {
    sails.log("la date a parser est : ", inputs.date)
      var parts = inputs.date.match(/(\d+)/g);
      sails.log(parts)
      // le constructeur de l'objet date en js prends an, mois, jour
    return exits.success(new Date(parts[2], parts[1]-1, parts[0]));
  }

};

