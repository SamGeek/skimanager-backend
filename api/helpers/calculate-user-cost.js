module.exports = {


  friendlyName: 'Calculate user cost',


  description: 'Ce helper me permets de calculer le cout actuel pour un utilisateur, inscrit pour un séjour donné',


  inputs: {
    // userId: {
    //   type: 'string',
    //   example: '',
    //   description: 'the user id we want to calculate the cost for',
    //   required: true
    // },

    stayId: {
      type: 'string',
      example: '',
      description: 'the related stay',
      required: true
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    // TODO

    //je recupere le stay pour commencer
    //var stay = await Stay.findOne({ id: inputs.stay, });

    sails.log('je veux voir lid de séjour qui a ete passe en parametre', inputs.stayId)

    var stays = await Stay.find({where: {id: inputs.stayId}}).populate('users').populate('usersys');
    var stay =  stays[0];
    
    
    //jai besoin de lid de lutilisateur dont on veut calculer la taxe de séjour

    //tous les userStay associés et je calcule le cout de tout ca
    var usersStay = stays[0].users
    var stayCost = 0

    sails.log('je veux voir les userstays', usersStay)

    //si le séjour est en gestion libre, de base ca fait 600euros
    stayCost = stay.stayType === 'pension_complete'  ? 0 :  600 //apres il peut y avoir des reductions a faire suivant le type d'utilisateur a voir plutard

    //la je dois faire un joli for pour les parcourir tous et popupler les bon champs

    for(const element of usersStay){

            //je fais un findOne pour voir si ca va marcher ou il va brailler coe dab
            var userStayList = await UserStay.find( {where: {id: element.id}}).populate('mealsPerDay').populate('skipasses');
            var userStay = userStayList[0];
            var mealsPerDay =  userStay.mealsPerDay;
            var skipasses =  userStay.skipasses;

            //il faut qu'on recuperère les bon prix en fonction du staytype
            var periodePricing = stay.stayType === 'pension_complete'  ? 'hiver' :  'ete'
            //periode et categorie sont les deux parametres qui constituent une clé primaire presque dans la table pricing

            //je dois faire un petit helper qui permet de calculer l'age de kkun et de retourner la bonne categorie dans laquelle il se trouve
            //ce helper doit prendre pour seul paramètre la date de Naissance du userStay
            var categoriePricing =  await sails.helpers.calculateUserAgeCategory.with({birthDate: userStay.dateNaissance});

            //en supposant ici que les deux paramètres sont renseignes, je recuperer le bon pricing
            var pricings = await Pricing.find({where: {periode: periodePricing, categorie: categoriePricing}});
            var pricing  = pricings[0];
            //il faut verifier qu'on recupere bien le bon pricing
            sails.log('je veux voir le séjour recupéré', pricing)

            //on doit rajouter pour chaque utilisateur le cout de l'adhesion
            if(element.adhesion ==='Adhérer pour un w-end') stayCost += pricing['adhesionWeekEnd']
            else if (element.adhesion ==='Adhérer au club') stayCost += pricing['adhesionSejour']
            //si le client est deja adherent on ne facture rien en plus
            

            //en supposant ici que nous avons le bon pricing, on va caculer le bon cout 
           

            //maintenant on doit parcourir les mealsPerDay et les skipasses
            
            try {
              //maintenant on mets ajour ce montant en parcourant les skipass
              for(const pass of skipasses){
                var skiPassKey = await sails.helpers.getSkipassKey.with({passName: pass.passName}); //get the appropraite key for the skipass, lui donner le parametre pass.passName
                //faire ce sous total dans un champ et le mettre a jour
                stayCost += pricing[skiPassKey]
              }

              //maintenant on mets a jour ce montant en parcourant les mealsPerDay
              for(const meal of mealsPerDay){
                //ici normalement sur le meal il devrait y avoir un attribut qui nous dis si cest en basse saison ou pas

                //si ce nest pas en basse saison, on fait ceci  //penser a faire un helper pour le calcul de la basse et de la haute saison
                //faire ce sous total dans un champ et le mettre a jour
                stayCost += meal.matin? pricing['chaletRepas'] : 0
                stayCost += meal.midi? pricing['chaletRepas'] : 0
                stayCost += meal.soir? pricing['chaletNuitee'] : 0

                //si non on fait ceci
                // stayCost += meal.matin? pricing['basseSaisonRepas'] : 0
                // stayCost += meal.midi? pricing['basseSaisonRepas'] : 0
                // stayCost += meal.soir? pricing['basseSaisonNuitee'] : 0
                sails.log('montant du stay cost apres ajout des prix des repas', stayCost)
              }

            } catch (err) {
              sails.log('erreur lors du traitement', err)
            }
            
    }

       //a la fin on retourne le resultat escompté
       return exits.success(parseFloat(parseFloat(stayCost).toFixed(2)));


    // ne pas oublier que en fonction du role de ce utilisateur certains couts peuvent changer


    //pas oublier de prendre en consideration les couts pour la bonne période


    //ne pas oublier de pouvoir categoriser les utilisateurs du séjour en fonction de leur age
    //un peti calcul doit se faire et il faut que cet age soit calculé par rapport au début du séjour (A clarifier avec Michèle)

  }


};

