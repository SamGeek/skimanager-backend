module.exports = {


  friendlyName: 'Get days between',


  description: 'Provide all the days between a startDate and an endDate',


  inputs: {

    startDateP: {
      type: 'string',
      example: '2020/07/03',
      description: 'The beginning of the stay',
      required: true
    },

    endDateP: {
      type: 'string',
      example: '2020/07/06',
      description: 'The end of the stay',
      required: true
    },

  },


  exits: {

    success: {
      outputFriendlyName: 'Days between',
    },

  },



  fn: async function(inputs,exits) {

    //le tableau qui va contenir la liste des jours dans lintervalle fourni en paramètre
    var days = [] 
    var startDate = await sails.helpers.parseDate(inputs.startDateP);
    var tomorrow = await sails.helpers.parseDate(inputs.startDateP);
    var endDate = await sails.helpers.parseDate(inputs.endDateP);
    sails.log("date de début", startDate);
    sails.log("date de fin", endDate);
    

    var timeDiff = Math.abs(startDate.getTime() - endDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

    for (let i = 0; i <= diffDays; i++) {
      var innerDate =  new Date(tomorrow)
      days.push(innerDate);
      innerDate = new Date(tomorrow.setDate(tomorrow.getDate() + 1));
    }
    return exits.success(days);
  },


};

