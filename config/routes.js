/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/



  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  'POST /user/login': 'user/login',
  'POST /user/verifyNumber': 'UserController.verifyNumber',
  'POST /user/addUserToStay': 'UserController.addUserToStay',
  'POST /user/fetchStaysOfUser': 'UserController.fetchStaysOfUser',
  'POST /user/fetchStaysOfUserTest': 'UserController.fetchStaysOfUserNew',
  'POST /organiser/addStay': 'UserController.addStay',
  'GET /organiser/fetchStays': 'UserController.fetchStayById',
  
  'GET /getDaysBetween': 'UserController.getDaysBetween',
  'GET /fillMeals': 'UserController.fillMeals',
  'GET /addSkiPass': 'UserController.addSkiPassToUserStay',

  //mise a jour des repas
  'POST /user/updateMeals': 'UserController.updateMeals',
  

  //Gestion du pricing
  'GET /fetchPricing': 'UserController.fetchPricingByPeriod',

  //routes admin
  'POST /admin/createUserAndAddToStay': 'WebAppController.createUserAndAddToStay',
  'POST /admin/addUserToStay': 'WebAppController.addUserToStay',

  //service pour tester le calcul du cout depuis le helper
  'GET /admin/testCostCompute': 'WebAppController.testCostCompute',

  //Services de gestion de la tarification
  'POST /admin/addDiscount': 'WebAppController.addDiscount',
  'POST /admin/addExtraCost': 'WebAppController.addExtraCost',
  'POST /admin/addDeposit': 'WebAppController.addDeposit',
  'POST /admin/fetchPayment': 'WebAppController.fetchPayment',
  'POST /admin/fetchGroupsOfStay': 'WebAppController.fetchGroupsOfStay',

  //Generatin stats
  'POST /admin/generateMealsStats': 'WebAppController.mealsStats',
  'POST /admin/deleteStay': 'WebAppController.deleteStay',

  //Billing services
  'POST /admin/saveBill': 'WebAppController.saveBill',
  'POST /admin/testParsing': 'WebAppController.testParsing',
  'POST /admin/fetchFinancialDetails': 'WebAppController.fetchFinancialDetails',

};
